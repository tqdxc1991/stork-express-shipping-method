<?php
/*
Plugin Name: Stork Express plugin
Plugin URI: https://stork.express/
Description: Stork Express method plugin
Version: 1.0.0
Author: SAS Stork Express
Author URI: https://stork.express/
*/
/*
Stork Express is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
Stork Express is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with Stork Express. If not, see https://stork.express/.
*/

/**
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	function stork_express_method_init() {
		if ( ! class_exists( 'WC_Stork_Express_Method' ) ) {
			class WC_Stork_Express_Method extends WC_Shipping_Method {
				/**
				 * Constructor for your shipping class
				 *
				 * @access public
				 * @return void
				 */
				public function __construct() {
					$this->id                 = 'stork_express_method'; // Id for your shipping method. Should be uunique.
					$this->method_title       = __( 'Stork Express Shipping Method' );  // Title shown in admin
					$this->method_description = __( 'Thanks to Woocommerce, now I have a great shipping method.
					I can configure it using the following configuration form.This shipping method will boost your sales!' ); // Description shown in admin

					$this->enabled            = "yes"; // This can be added as an setting but for this example its forced enabled
					$this->title              = "Stork Express Shipping Method"; // This can be added as an setting but for this example its forced.

					$this->init();
				}

				/**
				 * Init your settings
				 *
				 * @access public
				 * @return void
				 */
				function init() {
					// Load the settings API
					$this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
					$this->init_settings(); // This is part of the settings API. Loads settings you previously init.

					// Save settings in admin if you have any defined
					add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
				}

				function init_form_fields()
				{
				$this->form_fields = array(
				'enabled' => array(
				'title' => 'Live mode',
				'type' => 'select',
				'default' => 'no',
				'description' => 'Use this module in live mode',
				'options' => array(
					'0' => 'no',
					'1' => 'yes',
			   ) 
				),
				'password' => array(
				'title' =>'Password',
				'type' => 'password',
				'default' => null
				),
				'email' => array(
				'title' => 'Email',
				'type' => 'text',
				'description' => 'Enter a valid email address',
				'default' => null
				),
				'preparation_time' => array(
					'title' => 'Preparation time',
					'type' => 'text',
					'description' => 'Enter a valid preparation time',
					'default' => '1:00'
					),
				'monday_start_time' => array(
					'title' => 'Monday start time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '10:00'
					),
				'monday_end_time' => array(
					'title' => 'Monday end time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '19:00'
					),
				'tuesday_start_time' => array(
					'title' => 'Tuesday start time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '10:00'
					),
				'tuesday_end_time' => array(
					'title' => 'Tuesday end time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '19:00'
					),
				'wednesday_start_time' => array(
					'title' => 'Wednesday start time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '10:00'
					),
				'wednesday_end_time' => array(
					'title' => 'Wednesday end time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '19:00'
					),
				'thursday_start_time' => array(
					'title' => 'Thursday start time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '10:00'
					),
				'thursday_end_time' => array(
					'title' => 'Thursday end time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '19:00'
					),
				'friday_start_time' => array(
					'title' => 'Friday start time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '10:00'
					),
				'friday_end_time' => array(
					'title' => 'Friday end time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '19:00'
					),
				'saturday_start_time' => array(
					'title' => 'Saturday start time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '10:00'
					),
				'saturday_end_time' => array(
					'title' => 'Saturday end time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '19:00'
					),
				'sunday_start_time' => array(
					'title' => 'Sunday start time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '10:00'
					),
				'sunday_end_time' => array(
					'title' => 'Sunday end time',
					'type' => 'text',
					'description' => 'Enter a valid working time',
					'default' => '19:00'
					),
				);
				}

				function admin_options() {
					?>
					<h2><?php _e('Stork Express Shipping Method'); ?></h2>
					<p><?php _e('Here you can set the shop open period for pickup and preparation time for stork express shipping.'); ?></p>
					<table class="form-table">
					<?php $this->generate_settings_html(); ?>
					</table> <?php
					}

				/**
				 * calculate_shipping function.
				 *
				 * @access public
				 * @param mixed $package
				 * @return void
				 */
				public function calculate_shipping( $package = array() ) {
					
					$string_url = '';
					$cost = 0;
				
					foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

						$product = $cart_item['data'];
						$product_id = $cart_item['product_id'];
						$name = $product->get_name();
						$width = $product->get_width();
						$weight = $product->get_weight();
						$height = $product->get_height();
						$depth = $product->get_length();
						$price = $product->get_price();
						$quantity = $cart_item['quantity'];
						$link = $product->get_permalink( $cart_item );

						$string_url .= 'packets[0][externalId]=' . $product_id . '&';
						$string_url .= 'packets[0][name]=' . $name . '&';
						$string_url .= 'packets[0][width]=' . $width . '&';
						$string_url .= 'packets[0][weight]=' . $weight . '&';
						$string_url .= 'packets[0][height]=' . $height . '&';
						$string_url .= 'packets[0][depth]=' . $depth . '&';
						$string_url .= 'packets[0][value]=' . $price . '&';
						$string_url .= 'packets[0][img_link]=' . $link . '&';
						

						}
						
						


					$string_url .= 'pick_up_lat=25.76500500&pick_up_lng=-80.24379700&delivery_location_type=text_address&';

					$address = WC()->cart->get_customer()->get_shipping_address();
					$address_2 = WC()->cart->get_customer()->get_shipping_address_2();
					$postcode = WC()->cart->get_customer()->get_shipping_postcode(); 
					$city = $package["destination"]["city"];

					$string_url .= 'delivery_address_1=' . $address . '&';
					$string_url .= 'delivery_address_2=' . $address_2 . '&';
					$string_url .= 'delivery_postcode=' . $postcode . '&';
					$string_url .= 'delivery_city=' . $city . '&';

					$string_url .= 'delivery_timestamp=1529050138&type=send_parcel&delivery_type=d_to_m&is_on_offer=0&delivery_span=national';

					$response = wp_remote_get( 'https://server-dev.stork.express:8080/fares/suggestions?'.$string_url);
					$body = wp_remote_retrieve_body( $response );
					$myobj=json_decode($body);
					
					// {"success":true,"httpStatusCode":200,"data":{"tomorrowRate":149,"afterTomorrowRate":76}}
					//$response = wp_remote_get( 'https://server-dev.stork.express:8080/fares/suggestions?packets[0][externalId]=8&packets[0][name]=Mug Today is a good day&packets[0][width]=0&packets[0][weight]=0&packets[0][height]=0&packets[0][depth]=0&packets[0][value]=11&packets[0][img_link]=http://127.0.0.1/prestashop/8-large_default/demo_13.jpg&pick_up_lat=25.76500500&pick_up_lng=-80.24379700&delivery_location_type=text_address&delivery_address_1=82 RUE DU JARDIN PUBLIC&delivery_address_2=&delivery_postcode=33000&delivery_city=33000 - BORDEAUX&delivery_timestamp=1529050138&type=send_parcel&delivery_type=d_to_m&is_on_offer=0&delivery_span=national' );
					//$myobj = json_decode('{"success":true,"httpStatusCode":200,"data":{"tomorrowRate":149,"afterTomorrowRate":76}}');
					
					
					$cost = $myobj->{'data'}->{'tomorrowRate'};

					$rate = array(			
						'label' => 'Stork Express',
						//'label' => $string_url,
						'cost' => $cost,
						'calc_tax' => 'per_item',
						'tomorrow_rate' => '149',
						'after_tomorrow_rate' => '76'		
					);

					// Register the rate
					$this->add_rate( $rate );
				}
			}
		}
	}



	add_action( 'woocommerce_shipping_init', 'stork_express_method_init' );

	function add_stork_express_method( $methods ) {
		$methods['stork_express_method'] = 'WC_Stork_Express_Method';
		return $methods;
	}

	add_filter( 'woocommerce_shipping_methods', 'add_stork_express_method' );


	add_action( 'woocommerce_shipping_init', 'stork_express_method_init' );
	
	//supprimer le zéro de fin dans le prix
	add_filter( 'woocommerce_price_trim_zeros', '__return_true' );




    




}


// https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,
// +Mountain+View,+CA&key=AIzaSyD_M88eSeUsd1ckOoyh_XF1o_F0vnJkJOY
//  results->geometry->location->lat
//  results->geometry->location->lng